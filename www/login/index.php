<?php
session_start();

function goback() {
  header('Location: /');
  exit();
}

if (isset($_GET['logout'])) {

  // Logout

  if (!empty($_SESSION['player'])) {

    $mt_socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    socket_connect($mt_socket,'127.0.0.1','29999');

    $cmd=json_encode(array('lua'=>"webchat.logout('".$_SESSION['player']."')",'id'=>'web_logout'))."\n";
    socket_write($mt_socket,$cmd,strlen($cmd));
    $reply=socket_read($mt_socket,1048576);

    socket_close($mt_socket);

  }

  // Delete session cookie
  $cp=session_get_cookie_params();
  setcookie(session_name(),'',0,$cp['path'],$cp['domain'],$cp['secure'],isset($cp['httponly']));
  // Delete and close session
  session_destroy();
  session_write_close();

  goback();

} elseif (!empty($_SESSION['player'])) {

  // Already logged in
  goback();

} elseif (!empty($_POST['player']) and !empty($_POST['pass'])) {

  // Login attempt

  // Sanitize $player,$passwd
  $player=preg_replace('/[^A-Za-z0-9-_]/','',$_POST['player']);  // Allow only A-Za-z0-9-_
  $player=substr($player,0,19);                                  // Max length 19 chars
  $player=rtrim($player," \n\r\t\v\0\\");                        // Remove some trailing special chars
  $passwd=addslashes($_POST['pass']);                            // Escape some special characters ('"\)
  $passwd=substr($passwd,0,99);                                  // Max length 99 chars
  $passwd=rtrim($passwd,"\n\r\t\v\0\\");                         // Remove some trailing special chars

  $mt_socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
  socket_connect($mt_socket,'127.0.0.1','29999');

  $cmd=json_encode(array('lua'=>"return webchat.login('".$player."','".$passwd."','".$_SERVER['REMOTE_ADDR']."')",'id'=>'web_login'))."\n";
  socket_write($mt_socket,$cmd,strlen($cmd));
  $reply=socket_read($mt_socket,1048576);

  socket_close($mt_socket);

  if (isset(json_decode($reply)->{'result'}[0]) and json_decode($reply)->{'result'}[0]=="auth-ok") {

    // Login successful
    $_SESSION['player']=$player;
    goback();

  } else {

    // Wrong player or password
    header("HTTP/1.1 401 UNAUTHORIZED");
    $retry=true;

  }

} elseif (isset($_GET['from'])) {

  header("HTTP/1.1 401 UNAUTHORIZED");

}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Minetest Web Chat Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/style.css">
</script>
</head>
<body>

<div class="wchat-content">

  <h3>Minetest Web Chat Login</h3>

  <table id="wchat-chatin"><tbody>
    <tr><td>
      <form method="POST" action="">
        <p>Please log in with your Minetest player name and password.</p>
        <p id="u">In case you did not set a password for your player yet please do it now, empty passwords are not allowed.</p>
        <p>Player name: </p><p><input type="text" required minlength="1" name="player" placeholder="Enter player name and press enter..." autofocus></p>
        <p>Password:    </p><p><input type="password" required minlength="1" name="pass" placeholder="Enter password and press enter..."></p>
<?php
if (!empty($retry)) {
  echo '          <p id="u">Login failed. Make sure you entered the correct player name/password and that you have the shout privilege. Please try again.</p>'."\n";
}
?>
        <p><input type="submit" value="Login"></p>
      </form>
    </td></tr>
  </tbody></table>

</div>

</body>
</html>
