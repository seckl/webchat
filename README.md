# Minetest Webchat Mod

This mod makes the [Minetest](https://www.minetest.net/) [in-game chat](https://wiki.minetest.net/Chat) accessible through a web browser. The mod basically:

* adds some functions to interact with the Minetest in-game chat over a network socket.
* modifies the /msg and /me chat commands to work with the web chat.
* saves the in-game chat and some player data in local files. A locally installed web server can access the data and send it to the web users.

## Limitations and known bugs

* Currently the mod is very simple. It can only be used from a locally running web server, because the web server needs access to some files written by the mod.
* The browsers actively poll the chat for new messages. This causes some network and server load when multiple users are logged in at the same time.

# Installation

* Install the Webchat mod as described in the [Minetest Wiki](https://wiki.minetest.net/Installing_Mods).
* To access the Webchat functions over a network socket you also need to install the [Mineysocket](https://github.com/miney-py/mineysocket) mod.
* Set up a web server (e.g. [Apache](https://httpd.apache.org/)) on the same machine and copy the files in the directory `www/` to the DocumentRoot directory (e.g. `/var/www/html/`).
* Point $webchatdir in index.php to the Webchat data directory. Of course you can also modify the look & feel of the web pages as you like.
* Since the web server user (e.g. `www-data`) needs read access to some files in the data directory, you may need to adjust the access rights accordingly.

# Usage

To use the web chat, you just have to log in with your Minetest player name and password, no additional account is needed. You must set a password for your player in the game, empty passwords are not allowed.

After logging in you see the public chat on the left and your direct messages on the right side. You can hide and unhide both by clicking on the buttons below.

Type your messages in the input line below the chats. The web chat works just like the [in-game chat](https://wiki.minetest.net/Chat#Sending_messages):

* Just type your message and hit enter for public messages
* `/msg <player> <message>` for a direct message (DM) to a single player
* `/me <message>` to prepend your player name in front of a public message

Your messages will be sent to all players in the game and in the web chat.

On the bottom of the window the server messages are shown, e.g. when a player joins the game or the web chat.

## Example

The following screenshots shows the web chat running on a desktop and a smartphone.

![Screenshots](screenshots.png)

# License

[LGPL-3.0-or-later](https://spdx.org/licenses/LGPL-3.0-or-later.html)

